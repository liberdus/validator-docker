# Liberdus Validator Docker

To find out more about Liberdus visit https://liberdus.com

Liberdus is the first decentralized application powered by Shardus: https://shardus.com

## Support

If you're planning to run a Liberdus node, be sure to join our Discord channel for support and updates:

https://discord.gg/eGGSpAs

Before you get stated, create an account at https://test.liberdus.com and register an email address to recieve some initial coins. You will need the coins for staking as described later.

Here's a link to a video going over this setup from the Shardus 2020 Q1 Update (use IP addresses provided here and not in the video):

https://www.youtube.com/watch?v=X-LKOcnvneI&t=1891s

## Setup

### Docker

Install Docker Engine for your platfrom:

https://docs.docker.com/engine/install/

On a Linux server you can run the following commands.

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
# Be sure to view the get-docker.sh file to make sure it is safe before running the following command.
sudo sh get-docker.sh
```

### Liberdus

Login to your Liberdus testnet account and look under Settings - Stake to start staking.

Pull and run the Liberdus validator node Docker image for your platform:

```bash
# Linux

mkdir -p ~/liberdus &&
docker run -d \
  --name=liberdus \
  --user=$(id -u):$(id -g) \
  --network=host \
  -v ~/liberdus:/basedir \
  registry.gitlab.com/liberdus/validator-docker

# Raspbian

mkdir -p ~/liberdus &&
sudo docker run -d \
  --name=liberdus \
  --user=$(id -u):$(id -g) \
  --network=host \
  -v ~/liberdus:/basedir \
  registry.gitlab.com/liberdus/validator-docker:raspberrypi
```

Visit `localhost:9999` in a browser to access the admin dashboard with the following credentials:

```
username: admin
password: liberdus
```

Be sure to change the admin password once you login.

Most settings can be set to 'auto'. Don't change the "Archiver Public Key".
Change only the following settings from the admin dashboard to connect to the Libedus test net (http://69.30.199.114:3000/):

```
Archiver IP:

  69.30.199.114

Monitor Reporting Endpoint:

  http://69.30.199.114:3000/api

Reward Address:

  <Your test.liberdus.com 'Recieve' address>
```

Be sure that you have staked the required coins at the "Reward Address". 

Once the configuration settings have been entered and coins staked, click `Start Node`.

Click the "Show Log" link to watch your node connect and sync to the network.

## Node Rewards

The node reward should be recieved at the Liberdus account provided in the `Reward Address` field. You should be able to view the reward transactions through a Liberdus UI.

## Stopping the Node

To stop the node visit `localhost:9999` in a browser to access the admin dashboard and just click the "Stop" button.

## Upgrading the Node Version

1. Stop the node from the admin dashboard
2. Run the commands:

docker stop liberdus; docker rm liberdus; docker images

docker rmi < IMAGE ID >

3. Remove the liberdus directory

rm -rf ~/liberdus

4. Pull and run the Liberdus validator node Docker image for your platform as described above.

