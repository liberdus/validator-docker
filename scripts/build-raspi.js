const execa = require('execa')

execa.commandSync(`docker run --privileged --rm docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64`, { stdio: [0, 1, 2] })
execa.commandSync(`systemctl restart docker`, { stdio: [0, 1, 2] })

try {
  execa.commandSync(`docker buildx build --platform=linux/arm/v7 -t registry.gitlab.com/liberdus/validator-docker:raspberrypi .`, { stdio: [0, 1, 2] })
} catch (err) {
  execa.commandSync(`sudo docker buildx build --platform=linux/arm/v7 -t registry.gitlab.com/liberdus/validator-docker:raspberrypi .`, { stdio: [0, 1, 2] })
}
