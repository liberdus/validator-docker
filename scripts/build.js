const execa = require('execa')

try {
  execa.commandSync(`docker build -t registry.gitlab.com/liberdus/validator-docker .`, { stdio: [0, 1, 2] })
} catch (err) {
  execa.commandSync(`sudo docker build -t registry.gitlab.com/liberdus/validator-docker .`, { stdio: [0, 1, 2] })
}
