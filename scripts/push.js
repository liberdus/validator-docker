const execa = require('execa')

try {
  execa.commandSync(`docker login registry.gitlab.com`, { stdio: [0, 1, 2] })
} catch (err) {
  execa.commandSync(`sudo docker login registry.gitlab.com`, { stdio: [0, 1, 2] })
}

try {
  execa.commandSync(`docker push registry.gitlab.com/liberdus/validator-docker`, { stdio: [0, 1, 2] })
} catch (err) {
  execa.commandSync(`sudo docker push registry.gitlab.com/liberdus/validator-docker`, { stdio: [0, 1, 2] })
}
