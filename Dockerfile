# NOTE: This Dockerfile compiles an image that uses Debian Stretch as its OS
#
# Build time is fast because the native modules used by our app
# (sodium-native, sqlite3) have precomiled binaries for Debian.
#
# However, the resulting image size is very large (~1.25GB).
#
# Useful for development

# Node.js LTS 12.x.x from Docker Hub
FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

# Install node_modules
RUN npm set unsafe-perm true
RUN npm install

# Pass a BASE_DIR env var to set the basedir to /basedir
RUN mkdir -p /basedir
ENV BASE_DIR /basedir

# Define run command
CMD [ "npx", "validator-launcher", "/basedir"]

# Expose port for launcher dashboard
EXPOSE 9999
# Expose ports for Liberdus app
EXPOSE 9001
EXPOSE 10001
