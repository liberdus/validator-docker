# Updating the `validator-docker` Docker Image

The `validator-docker` project packs together a Liberdus validator ([`server`](https://gitlab.com/liberdus/server)) and a web-based admin dashboard ([`validator-launcher`](https://gitlab.com/liberdus/tools/validator-launcher)) as a ready-to-deploy Docker image.

The Docker image is hosted in this projects [container registry](https://gitlab.com/liberdus/validator-docker/container_registry). 

## Scripts

To update the Docker image, use the scripts provided in this projects [`package.json`](https://gitlab.com/liberdus/validator-docker/-/blob/master/package.json):

* `update` - Runs `build` then `push`.
* `update-raspi` - Runs `build-raspi` then `push-raspi`.
* `build` - Builds this projects Docker image and tags it as `latest`.
* `push` - Logs into the Gitlab container registry and pushes the `latest` image to it.
* `build-raspi` - Builds this projects Docker image for Raspberry Pi (`armv7`) and tags it as `raspberrypi`.
* `push-raspi` - Logs into the Gitlab container registry and pushes the `raspberrypi` image to it.

## Prereqs

To run the `build` script, you must have Docker Engine installed:

https://docs.docker.com/engine/install/

To run the `build-raspi` script, the experimental `buildx` feature must be enabled:

https://docs.docker.com/buildx/working-with-buildx/